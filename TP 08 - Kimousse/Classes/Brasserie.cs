﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_08___Kimousse.Classes
{
    class Brasserie
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Ville { get; set; }
        public int IdRegion { get; set; }
    }
}
